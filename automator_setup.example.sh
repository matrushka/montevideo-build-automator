#!/usr/bin/env bash

export MMA_WORK_DIR
export MMA_FLATTENER_DIR
export MMA_JEKYLL_DIR
export site_destination

MMA_WORK_DIR=mma_work_dir
MMA_FLATTENER_DIR=flattener
MMA_JEKYLL_DIR=jekyll
site_destination=~/preview.miraquetemiro.org/httpdocs

if [ ! -d "$MMA_WORK_DIR" ]; then
  echo "Creating work directory $MMA_WORK_DIR"
  mkdir mma_work_dir
fi
