# Montevideo Automator

Esta herramienta sirve para automatizar la descarga de información usando la herramienta Flattener, y después generar el sitio usando el generador basado en Jekyll. Además de ejecutar los comandos correspondientes a ambas acciones, guarda un _pidfile_ para permitir a otros procesos identificar si está en ejecución, y guarda archivos de log y error para poder depurar errores de ejecuciones pasadas.

## Requerimientos

* Ruby
* Ruby Bundler gem
* Jekyll gem
* Composer
* Git

## Instalación
Para iniciar es necesario instalar las herramientas basadas en Flattener y Jekyll.

`cp automator_setup.example.sh automator_setup.sh`

Ahí, configurar el directorio de destino del sitio web generado en la variable `$site_destination`. Suponiendo que no se alteren en `automator_setup.sh` las rutas para jekyll y flattener, a continuación:

```
git clone git@bitbucket.org:matrushka/montevideo-front.git jekyll
cd jekyll
git checkout release
git submodule update --init --recursive
bundle install --path vendor/bundle
mkdir data
```

```
# Regresar al directorio raíz de automator
cd .. 
git clone git@bitbucket.org:matrushka/montevideo-flattener.git flattener
cd flattener
composer install
ln -s ../jekyll/data data
```

## Ejecución
`./automator.sh`

## Registros de error y salida

El script crea un directorio (cuyo nombre se define en automator_setup.sh) en el que depositará dos archivos: automator.out y automator.err, con output y errores, respectivamente. Además, genera un archivo automator.pid, que destruye al terminar su ejecución (exitosa o no), y que es usado para determinar si hay una instancia de automator en ejecución.