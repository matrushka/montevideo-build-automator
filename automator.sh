#!/usr/bin/env bash

# Automator
# Ejecuta la descarga de información con la herramienta basada en Flattener
# Genera el sitio con la herramienta basada en Jekyll
# Además, genera un pidfile para identificar si se está ejecutando
# de tal forma que otras herramientas (la interfaz web) puedan determinar si 
# se puede llamar nuevamente, o no.
#
# Además, guarda registro de errores y mensajes informativos (stderr y stdout) 
# en archivos que se pueden leer posteriormente para depurar fallos

# Run the automator setup, where the configuration following environment
# variables should be set
# MMA_WORK_DIR

source automator_setup.sh
origin=`pwd`

# Check if the pidfile exists in the workdir. If it does, abort
if [ -e "$MMA_WORK_DIR/automator.pid" ]; then
  (>&2 echo "Automator is already running. Please wait for it to finish.")
  exit 1
fi

# Create the pid file with the pid of the current script
touch $MMA_WORK_DIR/automator.pid
echo $BASHPID > $MMA_WORK_DIR/automator.pid

# (re)create the error and log files
error_file="$origin/$MMA_WORK_DIR/automator.err"
log_file="$origin/$MMA_WORK_DIR/automator.out"

if [ -e "$error_file" ]; then
  rm $error_file
fi

if [ -e "$log_file" ]; then
  rm $log_file
fi

touch $error_file
touch $log_file



# Execute the Flattener export script, redirect errors and output to their respective files 
cd $origin
cd $MMA_FLATTENER_DIR

./export.php -y >> $log_file 2>> $error_file

if (( $? )); then
  echo "The execution of Flattener failed" >> $log_file  
  cd $origin
  rm "$MMA_WORK_DIR/automator.pid"
  exit 1
fi




# Execute the Jekyll command, redirect errors and output to their respective files
cd $origin
cd $MMA_JEKYLL_DIR

jekyll build --destination $site_destination >> $log_file 2>> $error_file

if (( $? )); then
  echo "The execution of Jekyll failed" >> $log_file  
  cd $origin
  rm "$MMA_WORK_DIR/automator.pid"
  exit 1
fi

# Copy the site preview over the live site
cd $origin
cd $MMA_WORK_DIR

rsync -a $site_destination/* $live_site_destination >> $log_file 2>> $error_file 

if (( $? )); then
  echo "The execution of rsync to publish the site failed" >> $log_file  
  cd $origin
  rm "$MMA_WORK_DIR/automator.pid"
  exit 1
fi




# Remove the pid file to allow the script to run again
cd $origin
rm "$MMA_WORK_DIR/automator.pid"
