#!/usr/bin/env bash

cd ~/build_automation/jekyll
unset GIT_DIR
git fetch origin
git reset --hard origin/release

cd ~/build_automation/flattener
unset GIT_DIR
git fetch origin
git reset --hard origin/master
